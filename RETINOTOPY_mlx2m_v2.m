%% Read trial information in 'TrialInfo' folder
%% EX specific paramters. (Exp infomation not in the log yet...)

HomeFolder=pwd;
fsep=filesep;
TrialInfo_folder=[HomeFolder,fsep,'TrialInfo',fsep];
Data_folder=[HomeFolder,fsep,'Data',fsep];
ROI_folder=[HomeFolder,fsep,'ImJROIs',fsep];
%% Read TIF metainfo

TiffMetaInfo=ReadTIFMeta_v2(Data_folder);
ImWidth=TiffMetaInfo.ImageInfo(2);
ImHeight=TiffMetaInfo.ImageInfo(1);
% program_rate = 10.0  %% acqurization framerate of Camera
Pixelpermm=162.3 ; %% Original resolution of camera is 155 pixel/mm.
PixelPermmInUse=60;
AreaperPixel=PixelPermmInUse^2;
warning off;

%% Read Trial Info.
TrialInfoMatF=dir([TrialInfo_folder,'*.mat']);

[RepN,TrialIdx,Tstart,Tstop] =importfile_RETINOTOPY(TrialInfo_folder);

%% load ROI file

% UseROI=input('Remove Outliers? Y/N ?','s');
UseROI='n';
if(strcmp(UseROI,'y'))
    ROImat=dir([ROI_folder,'*.mat']);
    for i=1:length(ROImat),
        disp([num2str(i),'. ',ROImat(i).name]);
    end;
    ROIidx=input('Choose :');
    ROI=load([ROI_folder,ROImat(ROIidx).name]);
    OutFrames=ROI.OutFrames;
    TOLs=0;
    for i=1:length(OutFrames),
        TOLs=TOLs+length(OutFrames{i}.OutIdx);
    end;
    disp([num2str(TOLs),' frames will be skipped']);
else
    ROIidx=-1;
end;
%%
%

MatFile=dir([TrialInfo_folder,fsep,'*.mat']);
load([TrialInfo_folder,fsep,MatFile.name]);
MonitorDistance=mon.currentCalib.distance;%15.5
MonitorWidth=mon.currentCalib.width;%%47.625
VisualAngle=abs(startPoint-endPoint);  %% span of display in visual angle
Repeat=double(Repeat);


% Repeat=10;   %% # of cycles per trial
% MonitorDistance=15.5
% MonitorWidth=47.625
% VisualAngle=90;  %% span of display in visual angle
TimDiff=diff(TimeKeeper);
MedianTimD=(max(TimDiff)+min(TimDiff))/2;
T_=TimDiff(TimDiff<MedianTimD);
T=mean(T_);
Repeat_int=uint8(Repeat);
Tstart=TimeKeeper(1:Repeat_int:end);
Tstop=TimeKeeper(Repeat_int:Repeat_int:end)+T;
% Tstop=Tstart+T*Repeat;


disp(['Mean Stimuli Period: ',num2str(T),' s with std ',num2str(std(T_)), ' s']);

BlankT_=TimDiff(TimDiff>MedianTimD)-T;
BlankT=mean(BlankT_);
disp(['Mean Blank Period: ',num2str(BlankT),' s with std ',num2str(std(BlankT_)), ' s']);

Ntrials=length(RepN);
Nblocks=max(unique(RepN))+1; %% zero based
NTrialPerBlock=Ntrials/Nblocks;  %% numer of EXP condition per block
disp([num2str(Nblocks),' blocks'])

ExpDur=Tstop(end)+BlankT;
disp(['Exp duration: ',num2str(ExpDur),' s']);

actual_rate = TiffMetaInfo.NTtlImg/ExpDur; % actual frame rate. should be VERY close to programm rate

disp(['Estimated FrameRate: ', num2str(actual_rate),' Hz']);
% actual_rate=program_rate;
% disp('using programm rate');

PhaseProcessionPerFrame=1/actual_rate/T*2*pi; %% phase processiong for during each camera frame

%% generate stimli code based on actual framerate.
ExpCons=unique(TrialIdx)'+1;
Ncons=length(ExpCons); %% numer of exp conditions

FrameCodeBase=ExpCons; %% simple list of TrialIndex: 1 based.
FrameCode=zeros(1,TiffMetaInfo.NTtlImg); % give each frame a label.

TstartFrame=round(Tstart*actual_rate);
CycleFrame=round(TimeKeeper*actual_rate);
FramePerCycle=round(T*actual_rate);
TstopFrame=round(Tstop*actual_rate);
% TstartFrame=ceil(Tstart*actual_rate);
% TstopFrame=floor(Tstop*actual_rate);

% FramePhase=zeros(length(RepN),FramePerCycle*Repeat);
FramePhase=[];
for i=1:length(RepN)
    
    FrameCode(TstartFrame(i): TstopFrame(i))= TrialIdx(i)+1; %% to sequentially parse each frame
    %     CycleFramesNow=CycleFrame((i-1)*Repeat+1:i*Repeat); %% actual imageing frame index during this trial (with n Cycles/repeat)
    %     %     NFrames=TstopFrame(i)-TstartFrame(i)+1;
    %     fidx=1;
    %     FramePhase_=[];
    %     for iRepeat=2:Repeat,  %% Within each repeat, assign the right phase (in radians) to each frame
    %         NFrames_r=CycleFramesNow(iRepeat)-CycleFramesNow(iRepeat-1) ; %% number of frames for this cycle
    %         curPhase=linspace(0,2*pi-2*pi/NFrames_r,NFrames_r);
    %         FramePhase_(fidx:fidx+NFrames_r-1)=curPhase;
    %         fidx=fidx+NFrames_r;
    %     end;
    %
    %     FramePhase_(fidx:fidx+NFrames_r-1)=curPhase;
    %
    %     FramePhase{i}=FramePhase_;
    
end;

%% verfiy FrameCode
% N0=length(find(FrameCode==0));
% for i=1:4,
%     length(find(FrameCode==i)),
% % end;

%% Get ave_f map
% disp('get average projection...');
% avg_projection=zeros(ImHeight,ImWidth);
% frame_F = zeros(1,TiffMetaInfo.NTtlImg);
% nFrame=1;
% for iFile=1:TiffMetaInfo.Nfiles,
%     if rem(iFile,10)==0
%         subroutine_progressbar_MH(iFile/TiffMetaInfo.Nfiles,0,'Getting Averge map:');
%     end
%     aFname = TiffMetaInfo.DirTIF(iFile).name;
%     if(strcmp(UseROI,'y')),
%         CurOutFrames=OutFrames{iFile}.OutIdx;
%     end;
%     disp(['Parsing ',aFname]);
%     AllFramesInFile=[];
%     AllFramesInFile = TIFFStack([Data_folder,aFname]);
%
%     xFrames=size(AllFramesInFile,3);
%
%     curFrame= sum(AllFramesInFile(:,:,:),3);
%
%     % average projection
%     avg_projection = avg_projection+curFrame;
%     %     % filter f_xy
%     %     f_xy = imfilter(curFrame,kernel,'same')/sum_kernel;
%     %     avg_f_xy = avg_f_xy+f_xy;
%
%     % mean fluorescence across frame
%     frame_F(nFrame:nFrame+xFrames-1) = squeeze(mean(mean(AllFramesInFile(:,:,:),2),1));
%     nFrame=nFrame+xFrames;
% end;
% subroutine_progressbar_MH(1);
% avg_projection=-avg_projection/nFrame;
avg_projection=0;
%% Visulization. Create my colormap

colorResolution=64;
colorCode=ones(colorResolution,3);
colorCode(:,1)=linspace(0,1,colorResolution);
colorCode=hsv2rgb(colorCode);
%% Trials segmentation
% if(isResize)
%     TheShort=ImHeight;
%     TheWide=ImWidth;
%     assert(round(TheWide)>=round(TheShort),'imaging dimension error: Short %f, Width %f',TheShort,TheWide);
%     % temp=imresize(zeros(ImHeight,ImWidth),PixelPermmInUse/Pixelpermm);
%     ImWidth=TheShort;
%     ImHeight=TheShort;
% %     avg_projection=imresize(avg_projection,[TheShort,TheShort]);
% end;



TotalFrame=0;
ConFrames=zeros(1,Ncons); %% register number of frames for each EXP condtion within one Block

Imag_phaseAcc= zeros(ImHeight,ImWidth,Nblocks,Ncons); %% for the phase map
Imag_ampAcc= zeros(ImHeight,ImWidth,Nblocks,Ncons);  %% for the amplitude map
Imag_ampAcc_blank= zeros(ImHeight,ImWidth);  %% for the amplitude map
NBlank=0;
F0=[];
F1=[];
PreExpcon=[];
TrialStart=0;
TrialCounter=1;
BlockCounter=[];
tic  %% takes long time...
for iFile=1:TiffMetaInfo.Nfiles,
    aFname = TiffMetaInfo.DirTIF(iFile).name;
    if(strcmp(UseROI,'y')),
        CurOutFrames=OutFrames{iFile}.OutIdx;
    end;
    disp(['Parsing ',aFname]);
    AllFramesInFile=[];
    AllFramesInFile = TIFFStack([Data_folder,aFname]);
    for iFrame=1:TiffMetaInfo.Nimages(iFile)
        
        TotalFrame=TotalFrame+1;  %% numer of frames parsed so far
        [~,~,Icon]=intersect(FrameCode(TotalFrame),FrameCodeBase); %% find the Trialcode for this frame. Ignore this frame if not in FrameCode Base
        if(isempty(Icon))
            if(TrialStart==1) %% What to do when finishing a Trial
                TrialStart=0; %% trial end
                disp(['Skipped frames: ',num2str(skippedFrame)]);
                F0=4096-Imag_ampAcc(:,:,BlockCounter,PreExpcon)/length(frameang);
                
                Imag_ampAcc(:,:,BlockCounter,PreExpcon) = F0;
                
                F1=Imag_phaseAcc(:,:,BlockCounter,PreExpcon) - F0*sum(exp(1i*frameang)); %Subtract F0 leakage
                Imag_phaseAcc(:,:,BlockCounter,PreExpcon) = 2*F1./(length(frameang)-1);  %%
                
                TrialCounter=TrialCounter+1; %% get ready for next Trial
                
            end;
            NBlank=NBlank+1;
            Imag_ampAcc_blank=Imag_ampAcc_blank+double(AllFramesInFile(:,:,iFrame));
            continue; %% ignore this frame if not in FrameCode Base
        else
            if(TrialStart==0)  %% if a Frame in FrameCodeBase is found, start a new Trial.
                TrialStart=1;  %% a new trial start
                skippedFrame=0;
                BlockCounter=floor((TrialCounter-1)/NTrialPerBlock)+1; %% Index of block
                
                %                 if(BlockCounter>0)
                %                     figure('Name',['Retinotopy map -Block: ',num2str(BlockCounter)]);
                %                     ct=1;
                %                     for i=[1,3,2,4]
                %                         subplot(3,2,i),
                %                         imagesc(angle(Imag_phaseAcc(:,:,BlockCounter,ct)));
                %                         axis square
                %                         title([mapName{ct},' @ Block ',num2str(BlockCounter)]);
                %                         colormap(colorCode);
                %                         ct=ct+1;
                %                     end;
                %                     H_map_=(angle(Imag_phaseAcc(:,:,BlockCounter,2))-angle(Imag_phaseAcc(:,:,BlockCounter,1)))/2;
                %                     V_map_=(angle(Imag_phaseAcc(:,:,BlockCounter4))-angle(Imag_phaseAcc(:,:,BlockCounter,3)))/2;
                %                     subplot(3,2,5),
                %                     imagesc(H_map_);axis square;colormap(colorCode);title('Horizontal Retinotopy');axis square;colormap(colorCode);
                %                     subplot(3,2,6),
                %                     imagesc(V_map_);axis square;colormap(colorCode);title('Vertical Retinotopy');axis square;colormap(colorCode);
                %                     shg;
                %                 end;
                %
                
                
                FrameC=0;  %% reset frame counter of this new trial
                disp(['@ Frame ',num2str(TotalFrame),'. Parsing Trial: ',num2str(TrialCounter),'/',num2str(Ntrials),'. Block:',num2str(BlockCounter),'/',num2str(Nblocks)]);
                %                 Nframes= TstopFrame(TrialCounter)-TstartFrame(TrialCounter)+1;  % number of total frames
                %                 frameang = linspace(0,2*pi*Repeat,Nframes); % phase of each coming frame within this Trial
                
                framest= (TstartFrame(TrialCounter): TstopFrame(TrialCounter))-TstartFrame(TrialCounter);  % frame index of image data at this Trial
                frameang = (framest/actual_rate)/T*2*pi;  % phase of each coming frame within this Trial
                %                frameang =mod(framest,FramePerCycle)/FramePerCycle*2*pi;  % phase of each coming frame within this Trial
                %
                %                 frameang=FramePhase{TrialCounter};
                
                %  frameang=linspace(0,Repeat*2*pi,length(framest));
                
            end;
        end;
        
        FrameC=FrameC+1;
        %         if(FrameC>length(frameang))
        %             skippedFrame=skippedFrame+1;
        %             continue;
        %         end;
        PreExpcon=Icon; %% for trialender triggered aveaging
        if(strcmp(UseROI,'y')),
            if(isempty(intersect(iFrame,CurOutFrames)))
                
                currframe = double(AllFramesInFile(:,:,iFrame));
            else
                currframe=zeros(ImHeight,ImWidth);
            end;
        else
            currframe = double(AllFramesInFile(:,:,iFrame));
        end;
        
        
        im=4096-currframe;
        
        %%% percent change
        %         im=(im-avg_projection)./avg_projection;
        %% Obtain and accumulate the phase for this frame
        Imag_phaseAcc(:,:,BlockCounter,Icon) = Imag_phaseAcc(:,:,BlockCounter,Icon) + exp(1i*frameang(FrameC)).*im;
        
        %% Accumulate raw amplitude
        Imag_ampAcc(:,:,BlockCounter,Icon) = Imag_ampAcc(:,:,BlockCounter,Icon) +currframe;
        %         else
        %             disp(['Skip Frame ',num2str(iFrame),' as outlier']);
        %         end;
        hbar=waitbar(TotalFrame/TiffMetaInfo.NTtlImg);
    end
end;
close(hbar);
toc

%% block averaged phase map;
% Imag_ampAcc_blank=Imag_ampAcc_blank/NBlank;  %% activity of blank

Image_phaseAcc_mean=squeeze(mean(Imag_phaseAcc,3));
% disp('just use the firt block');
% Image_phaseAcc_mean=squeeze(Imag_phaseAcc(:,:,1,:));



H_mapR=(angle(Image_phaseAcc_mean(:,:,2))-angle(Image_phaseAcc_mean(:,:,1)))/2;


H_mapR=H_mapR*180/pi; %% to degree

V_mapR=(angle(Image_phaseAcc_mean(:,:,4))-angle(Image_phaseAcc_mean(:,:,3)))/2;
V_mapR=V_mapR*180/pi; %% to degree
disp('OK');
%% downsampe
 PixelPermmInUse=40
Image_phaseAcc_mean2=[];
for i=1:4,
    Image_phaseAcc_mean2(:,:,i)=imresize(squeeze(Image_phaseAcc_mean(:,:,i)), PixelPermmInUse/Pixelpermm);
end;
% Image_phaseAcc_mean2=squeeze(mean(Imag_phaseAcc,3));
H_map=(angle(Image_phaseAcc_mean2(:,:,2))-angle(Image_phaseAcc_mean2(:,:,1)))/2;

H_map=H_map*180/pi; %% to degree

V_map=(angle(Image_phaseAcc_mean2(:,:,4))-angle(Image_phaseAcc_mean2(:,:,3)))/2;
V_map=V_map*180/pi; %% to degree
disp('OK');
%%
if(~isdir('Results'))
    cd(HomeFolder);
    mkdir('Results');
end;

% save([HomeFolder,'\Results\',TiffMetaInfo.SessionName],'HomeFolder','TiffMetaInfo','MonitorDistance','Pixelpermm',...
%     'MonitorWidth','Imag_phaseAcc','Imag_ampAcc','H_map','V_map','ImWidth','ImHeight','VisualAngle','ROIidx')
save([HomeFolder,fsep,'Results',fsep,TiffMetaInfo.SessionName],'colorCode','Nblocks','HomeFolder','TiffMetaInfo','MonitorDistance','actual_rate','PixelPermmInUse','Pixelpermm',...
    'MonitorWidth','actual_rate','Imag_phaseAcc','Imag_ampAcc','H_map','V_map','H_mapR','V_mapR','fsep','ImWidth','ImHeight','VisualAngle')
disp('Data saved');
%% Create Horizontal and Vertical phase map for each block
clear MapSereno V_map_accum H_map_accum VFS
mapName={'H-Left','H-Right','V-Up','V-Down',};

for bindex=1:Nblocks,
    
    figure('Name',['Retinotopy map -Block: ',num2str(bindex)]);
    ct=1;
    for i=[1,3,2,4]
        subplot(3,2,i),
        imagesc(angle(Imag_phaseAcc(:,:,bindex,ct)));
        axis square
        title([mapName{ct},' @ Block ',num2str(bindex)]);
        colormap(colorCode);
        ct=ct+1;
    end;
    H_map_=(angle(Imag_phaseAcc(:,:,bindex,2))-angle(Imag_phaseAcc(:,:,bindex,1)))/2;
    V_map_=(angle(Imag_phaseAcc(:,:,bindex,4))-angle(Imag_phaseAcc(:,:,bindex,3)))/2;
    subplot(3,2,5),
    imagesc(H_map_);axis square;colormap(colorCode);title('Horizontal Retinotopy');axis square;colormap(colorCode);
    subplot(3,2,6),
    imagesc(V_map_);axis square;colormap(colorCode);title('Vertical Retinotopy');axis square;colormap(colorCode);
    
    H_map_accum(:,:,bindex)=H_map_;
    V_map_accum(:,:,bindex)=V_map_;
    
    
    %     V_map_=imresize(V_map_,PixelPermmInUse2/Pixelpermm);
    %     H_map_=imresize(H_map_,PixelPermmInUse2/Pixelpermm);
    
    %     V_map1=V_map_;
    %     H_map1=H_map_;
    FilterSD=3;
    NthrSD=1.5;
    
    %     %%% transform from monitor location to sperical altitude and azimuth (see Marshel 2012)
    V_mapL=V_map_/360*MonitorWidth/1.6;  %% 1.6: aspect ratio of montior.
    H_mapL=H_map_/360*MonitorWidth/1.6;
    
    R=sqrt(MonitorDistance^2+V_mapL.^2+H_mapL.^2);
    V_map_transform=90-acosd(V_mapL./R);
    H_map_transform=atand(H_mapL/MonitorDistance);
    %
    %      [MapSereno(:,:,bindex),VFS(:,:,bindex),hSereno,xdom,ydom]=getSerenoMap(H_map_transform,V_map_transform,PixelPermmInUse,FilterSD,NthrSD);
    %       [MapCallaway,hCallaway,AreaInfo]=getCallawayMap(H_map_transform,V_map_transform,PixelPermmInUse,FilterSD,NthrSD);
    %        [MapSereno(:,:,bindex),VFS(:,:,bindex),hSereno,xdom,ydom]=getSerenoMap(H_map1,V_map1,Pixelpermm,FilterSD,NthrSD);
    %    set(hSereno,'Name',[TiffMetaInfo.SessionName,' Block: ',num2str(bindex)]);
end;
H_map_accum_avg=circ_mean(H_map_accum,[],3);
V_map_accum_avg=circ_mean(V_map_accum,[],3);

figure;

%% Retinotopy map based on Sereno and Callaway's methods

% if(ImWidth~=ImHeight)
%     disp('Dimension mismatch');
%     disp('check your imaging setting....')
%     return;
% end;

%% transform from monitor location to sperical altitude and azimuth (see Marshel 2012)
%% V_mapL is the distance measured on monitor
%% 1.6: aspect ratio of montior. so MonitorWidth/1.6 gives monitor height.
%% as visual stimli is confined between [-monitorwidth/2 -montiorwidth/2]
V_map1=V_map;
H_map1=H_map;
V_mapL=V_map1/360*MonitorWidth/1.6;
H_mapL=H_map1/360*MonitorWidth/1.6;

R=sqrt(MonitorDistance^2+V_mapL.^2+H_mapL.^2);
V_map_transform=90-acosd(V_mapL./R);
H_map_transform=atand(H_mapL/MonitorDistance);
disp('OK');
%%
V_mapL2=V_mapR/360*MonitorWidth/1.6;
H_mapL2=H_mapR/360*MonitorWidth/1.6;
R=sqrt(MonitorDistance^2+V_mapL2.^2+H_mapL2.^2);
V_map_transform2=90-acosd(V_mapL2./R);
H_map_transform2=atand(H_mapL2/MonitorDistance);
%%

save([HomeFolder,fsep,'Results',fsep,TiffMetaInfo.SessionName],'V_map_transform','H_map_transform','V_map_transform2','H_map_transform2','-append')
% getMouseAreasX(H_map_transform,V_map_transform,Pixelpermm,FilterSD,NthrSD);
%% simple scaling if no sperical correction

% V_map_transform=V_map;
% H_map_transform=H_map;
% V_map_transform=V_map*VisualAngle/180;
% H_map_transform=H_map*VisualAngle/180;
%%%% Get Sereno map first, as it is always available.
%%
%

% close all;
%% default.

FilterSD=3
NthrSD=1.3;
disp('OK');


[MapSereno,VFS_TEMP,hSereno,xdom,ydom]=getSerenoMap(H_map_transform,V_map_transform,PixelPermmInUse,FilterSD,NthrSD);
%  [MapCallaway,hCallaway,AreaInfo]=getCallawayMap(H_map_transform,V_map_transform,PixelPermmInUse,FilterSD,NthrSD);
set(hSereno,'Name',[TiffMetaInfo.SessionName,' Sereno Map']);
subplot(221);
title(['FilterSD= ',num2str(FilterSD),' NthrSD= ',num2str(NthrSD)])
saveas(hSereno,[HomeFolder,fsep,'Results',fsep,TiffMetaInfo.SessionName,'_SerenoMap'],'fig');
save([HomeFolder,fsep,'Results',fsep,TiffMetaInfo.SessionName],'MapSereno','xdom','ydom','FilterSD','-append')
%%%% Get Calaway map
MapCallaway=[];
try  %% not always works... Sensitive to parameters
    [MapCallaway,MapCallaway_preSplit, hCallaway,AreaInfo,AreaInfo_bfs]=getCallawayMap(H_map_transform,V_map_transform,PixelPermmInUse,FilterSD,NthrSD);
catch
    disp('*******************************************')
    disp('WARNING!!! Error')
    disp('*******************************************')
end
save([HomeFolder,fsep,'Results',fsep,TiffMetaInfo.SessionName],'HomeFolder','MapCallaway','AreaInfo','-append')
set(hCallaway,'Name',TiffMetaInfo.SessionName);
saveas(hCallaway,[HomeFolder,fsep,'Results',fsep,TiffMetaInfo.SessionName,'_CallawayMap'],'fig');
%%


