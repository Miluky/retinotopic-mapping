function [V1id ids V1map] = getV1id_MH(im)

imlabel = bwlabel(im,4);
imdom = unique(imlabel);

%% edited by ming
% imlabel=max(max(imlabel))-imlabel;
%%
clear Sqmm
sqmm=zeros(1,length(imdom));
for q = 2:length(imdom)
    Sqmm(q) = length(find(imlabel == q-1))/(39^2); %cortical area coverage
end
[dum V1id] = max(Sqmm);
ids = find(imlabel == V1id);

V1map = zeros(size(im));
V1map(ids) = 1;