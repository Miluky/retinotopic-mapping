function [imSereno,hSereno,xdom,ydom] = getSerenoMap(kmap_hor,kmap_vert,pixpermm)
%% INPUTS
%kmap_hor - Map of horizontal retinotopic location, x
%kmap_vert - Map of vertical retinotopic location
%pixpermm = mm/pix of the retinotopy images
%colorCode: custom colormap

%%% comment from Ian. I don't quite get it.
% The images in Garrett et al '14 were collected at 39 pixels/mm.  It is
% recommended that kmap_hor and kmap_vert be down/upsampled to this value
% before running.

%% OUTPUTS

% imSereno is the binary image for the intial segmentation (based on Sereno map only)
% hSereno: figure handle. for further plotting if applicable
% xdom, ydom: meshgrid for plotting
%% Compute visual field sign map

mmperpix = 1/pixpermm;

[dhdx dhdy] = gradient(kmap_hor);
[dvdx dvdy] = gradient(kmap_vert);

graddir_hor = atan2(dhdy,dhdx);
graddir_vert = atan2(dvdy,dvdx);

vdiff = exp(1i*graddir_hor) .* exp(-1i*graddir_vert); %Should be vert-hor, but the gradient in Matlab for y is opposite.
VFS = sin(angle(vdiff)); %Visual field sign map
id = find(isnan(VFS));
VFS(id) = 0;

hh = fspecial('gaussian',size(VFS),3);
hh = hh/sum(hh(:));
VFS = ifft2(fft2(VFS).*abs(fft2(hh)));  %Important to smooth before thresholding below


%% Plot retinotopic maps

xdom = (0:size(kmap_hor,2)-1)*mmperpix;
ydom = (0:size(kmap_hor,1)-1)*mmperpix;

hSereno=figure('Name','Sereno Map');
clf
% subplot(2,4,1)
% % imagesc(xdom,ydom,kmap_hor,[-VisualAngle/2 VisualAngle/2]),
% imagesc(xdom,ydom,kmap_hor)
% axis image, colormap('jet');freezeColors; h=colorbar;cbfreeze(h);
% title('1. Horizontal (azim deg)')
% 
% subplot(2,4,2)
% % imagesc(xdom,ydom,kmap_vert,[-VisualAngle/2 VisualAngle/2]),
% imagesc(xdom,ydom,kmap_vert)
% axis image, colormap('jet');freezeColors; h=colorbar;cbfreeze(h);
% title('2. Vertical (alt deg)')

%% Plotting visual field sign and its threshold

figure(hSereno), subplot(2,2,1),
imagesc(xdom,ydom,VFS,[-1 1]),
axis image, colormap('jet');freezeColors; h=colorbar;cbfreeze(h);
title('Sereno sign map')
xlabel('mm');ylabel('mm')

gradmag = abs(VFS);
figure(hSereno), subplot(2,2,2),

Thr=1.5;
threshSeg = Thr*std(VFS(:));
imseg = (sign(gradmag-threshSeg/2) + 1)/2;  %threshold visual field sign map at +/-1.5sig

id = find(imseg);
imdum = imseg.*VFS; imdum(id) = imdum(id)+1.1;
ploteccmap(imdum,[.1 2.1],pixpermm);
colorbar off
axis image, colormap('jet');freezeColors;
xlabel('mm');ylabel('mm')
title(['+/-',num2str(Thr),'xSig = ' num2str(threshSeg)])


patchSign = getPatchSign(imseg,VFS);

% figure(hSereno), subplot(2,4,5),
% ploteccmap(patchSign,[1.1 2.1],pixpermm);
% title('watershed')
% axis image
% colorbar off
% title('5. Threshold patches')

id = find(patchSign ~= 0);
patchSign(id) = sign(patchSign(id) - 1);

SE = strel('disk',2,0);
imseg = imopen(imseg,SE);

patchSign = getPatchSign(imseg,VFS);

% figure(hSereno), subplot(2,4,6),
% ploteccmap(patchSign,[1.1 2.1],pixpermm);
% title('watershed')
% colorbar off
% title('6. "Open" & set boundary')



%% Make boundary of visual cortex

%First pad the image with zeros because the "imclose" function does this
%wierd thing where it tries to "bleed" to the edge if the patch near it

Npad = 512;  %Arbitrary padding value. 30. May need more depending on image size and resolution
dim = size(imseg);
imsegpad = [zeros(dim(1),Npad) imseg zeros(dim(1),Npad)];
dim = size(imsegpad);
imsegpad = [zeros(Npad,dim(2)); imsegpad; zeros(Npad,dim(2))];

SE = strel('disk',10,0);
imbound = imclose(imsegpad,SE);

imbound = imfill(imbound); %often unnecessary, but sometimes there are small gaps need filling

% SE = strel('disk',5,0);
% imbound = imopen(imbound,SE);

SE = strel('disk',3,0);
imbound = imdilate(imbound,SE); %Dilate to account for original thresholding.
imbound = imfill(imbound);

%Remove the padding
imbound = imbound(Npad+1:end-Npad,Npad+1:end-Npad);
imbound(:,1) = 0; imbound(:,end) = 0; imbound(1,:) = 0;  imbound(end,:) = 0;

%Only keep the "main" group of patches. Preveiously used opening (see above), but this is more robust:
bwlab = bwlabel(imbound,4);
labid = unique(bwlab);
for i = 1:length(labid)
    id = find(bwlab == labid(i));
    S(i) = length(id);
end
S(1) = 0; %To ignore the "surround patch"
[dum id] = max(S);
id = find(bwlab == labid(id));
imbound = 0*imbound;
imbound(id) = 1;
imseg = imseg.*imbound;

%This is important in case a patch reaches the edge... we want it to be
%smaller than imbound
imseg(:,1:2) = 0; imseg(:,end-1:end) = 0; imseg(1:2,:) = 0;  imseg(end-1:end,:) = 0;



%% Morphological thinning to create borders that are one pixel wide

%Thinning
bordr = imbound-imseg;
bordr = bwmorph(bordr,'thin',Inf);
bordr = bwmorph(bordr,'spur',4);

%Turn border map into patches
im = bwlabel(1-bordr,4);
im(find(im == 1)) = 0;
im = sign(im);

% bwlab = bwlabel(im,4);
% labid = unique(bwlab);
% for i = 1:length(labid)
%    id = find(bwlab == labid(i));
%    if length(id) < 30
%        im(id) = 0;
%    end
% end

imSereno=im; 
%% Plot stuff

patchSign = getPatchSign(im,VFS);

figure(hSereno), subplot(2,2,3),
ploteccmap(patchSign,[1.1 2.1],pixpermm);
hold on,
contour(xdom,ydom,im,[.5 .5],'k')
title('"Thinning"')
colorbar off
xlabel('mm');ylabel('mm')

%% Plot eccentricity map, with [0 0] defined as V1's center-of-mass

SE = strel('disk',10);
imdum = imopen(imseg,SE);
[CoMxy Axisxy] = getPatchCoM(imdum);
% [CoMxy Axisxy] = getPatchCoM_MH(imdum);

%  V1id = getV1id_MH2(imdum,pixpermm);
V1id = getV1id(imdum);

AreaInfo.Vcent(1) = kmap_hor(round(CoMxy(V1id,2)),round(CoMxy(V1id,1)));  %Get point in visual space at the center of V1
AreaInfo.Vcent(2) = kmap_vert(round(CoMxy(V1id,2)),round(CoMxy(V1id,1)));

az = (kmap_hor - AreaInfo.Vcent(1))*pi/180; %azimuth
alt = (kmap_vert - AreaInfo.Vcent(2))*pi/180; %altitude
AreaInfo.kmap_rad = atand(  sqrt( tan(az).^2 + (tan(alt).^2)./(cos(az).^2)  )  );  %Eccentricity

figure(hSereno),subplot(2,2,4)
ploteccmap(AreaInfo.kmap_rad.*im,[0,90],pixpermm);
%imagesc(xdom,ydom,AreaInfo.kmap_rad.*im);
hold on
contour(xdom,ydom,im,[.5 .5],'k')
axis image
title('Eccentricity map')
xlabel('mm');ylabel('mm')


function imout = ploteccmap(im,rngColorBar,pixpermm)

%This assumes that the zeros are the background
mmperpix = 1/pixpermm;

xdom = (0:size(im,2)-1)*mmperpix;
ydom = (0:size(im,1)-1)*mmperpix;

bg = ones(size(im));
bgid = find(im == 0);
bg(bgid) = 0;

im(find(im>rngColorBar(2))) = rngColorBar(2);
im = im/rngColorBar(2);

im = round(im*63+1);

im(bgid) = NaN;

dim = size(im);
jetid = jet;
imout = zeros(dim(1),dim(2),3);
for i = 1:dim(1)
    for j = 1:dim(2)
        
        if isnan(im(i,j))
            imout(i,j,:) = [1 1 1];
        else
            imout(i,j,:) = jetid(im(i,j),:);
        end
    end
end


image(xdom,ydom,imout), axis image

imout_min=min(min(min(imout)));
imout_max=max(max(max(imout)));
caxis([imout_min,imout_max]);
h=colorbar;

barlim=get(h,'Ytick');

eccdom = round(linspace(rngColorBar(1),rngColorBar(2),5));
for i = 1:length(eccdom)
    domcell{i} = eccdom(i);
end
TicLoc=double(linspace(imout_min,imout_max,5));
colorbar('YTick',TicLoc,'YTickLabel',domcell)


function [patchSign areaSign] = getPatchSign(im,imsign)

imlabel = bwlabel(im,4);
areaID = unique(imlabel);
patchSign = zeros(size(imlabel));
for i = 2:length(areaID)
   id = find(imlabel == areaID(i));
   m = mean(imsign(id));
   areaSign(i-1) = sign(m);
   patchSign(id) = sign(m)+1.1;
end