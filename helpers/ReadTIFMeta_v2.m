function TiffMetaInfo=ReadTIFMeta_v2(dataDirPath)
%% Function to read TIF meta information.
%%% input: folder name where Tiff is stored. Open a digolog if empty.
%%% output: a structure with critical information.

%%
% if nargin < 1
%     [~, TiffFolder] = uigetfile('*.tif', 'Pick a MATLAB code file');
% end
% 
% cd(TiffFolder);

DirTIF=dir([dataDirPath '/*.tif']);
Nfiles = length(DirTIF);
Nimages = zeros(Nfiles, 1);
NTtlImg = 0;
kidx=[];
ImageInfo=zeros(1,3);
numStr=200;
for i=1:Nfiles,
    
    if(length(DirTIF(i).name)<numStr);
        numStr=length(DirTIF(i).name);
        SessionName=DirTIF(i).name;
    end;
end;
SessionName=SessionName(1:end-4)
Cycleidx=strfind(SessionName,'_Cycle'); %% check if it is file from 2p
TifFileName=cell(1,Nfiles);
for i=1:Nfiles,
    if(i==1)
        TifFileName{i}=[SessionName,'.tif'];
    else
        TifFileName{i}=[SessionName,'_',num2str(i-1),'.tif'];
    end;
end;
warning off;
disp('Reading TIFF Meta info...')
if(isempty(Cycleidx))
   
 
    for j=1:Nfiles
        
        aFname = TifFileName{j}

%         if(j>1)
%             if(~isempty(Cycleidx))
%                 break;
%             end;
%         end;
        tsStack=TIFFStack([dataDirPath aFname]);
        ImageInfo_temp = size(tsStack);
        Nimages(j)    = ImageInfo_temp(3); % number of frames in this stack
%         NTtlImg       = Nimages(j) + NTtlImg;  % total frames
        if(j==1)
        ImageInfo(j,:)=ImageInfo_temp; 
        end;
    end
    NTtlImg=sum(Nimages);
   
else
    
    Nfiles = length(DirTIF);
    ImageInfo = imfinfo(DirTIF(1).name);
end;



TiffMetaInfo=StructureConstructor(SessionName,TifFileName,Nfiles,Nimages,NTtlImg,ImageInfo);
%% display info.
disp(['SessionName: ', SessionName]);
disp(['Number of files: ',num2str(Nfiles)]);
if(isempty(Cycleidx))
    disp(['Total frames: ',num2str(NTtlImg)]);
end;

% cd ..